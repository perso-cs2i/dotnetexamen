﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Examen.Model;
using System;
using System.Linq;
using Examen.Ext;

namespace ExtensionTests
{


    [TestClass]
    public class EvenementTest
    {
        private Proprietaire p1;
        private Proprietaire p2;

        private Invite i1;
        private Invite i2;
        private Invite i3;
        private Invite i4;

        private string[] _tags = {"Home", "Work", "Sport", "Travel","Cinema", "Music"};

        private int[] _values = { 10, 20, 30, 47, -20 };
        private List<int> _values2 = new List<int> { -50, -20, 0, 10, 15, 18, 20, 22, 32, 30, 40, 45, 50, 60 };

        #region Initializations
        private List<Evenement> events;

        [TestInitialize]
        public void Initialize()
        {

            p1 = new Proprietaire("Celine", "Lescop", "cl@gmail.com", true);
            p2 = new Proprietaire("Herve", "Inisan", "hi@gmail.com", true);

            i1 = new Invite("a", "aa", "aaa@gmail.com", false);
            i2 = new Invite("b", "bb", "bbb@gmail.com", false);
            i3 = new Invite("c", "cc", "ccc@gmail.com", false);
            i4 = new Invite("d", "dd", "ddd@gmail.com", false);


           
        
        var date = new DateTime(2021, 05, 21); 
            var date2 = new DateTime(2021, 05, 23);

            var date3 = new DateTime(2021, 07, 21);
            var date4 = new DateTime(2021, 07, 23);


            events = new List<Evenement>();
            events.Add(new Evenement(p1,"e1", "blblbl",date, date2,true,_tags, i1));
            events.Add(new Evenement(p2, "e2", "blblbl", date3, date4, true, _tags, i3));

        }

        #endregion

       
        /*[TestMethod]
        public void TestMai2021()
        {

            Func<Evenement, bool> filtre = p => p.StartDate && p.EndDate;
            Func<Evenement, DateTime> tri = p => p.StartDate;
            var resultats = events
            .Where(filtre)
            .OrderBy(tri);
        }*/


        [TestMethod]
        public void TestEventsInisanConfirmed()
        {
            Func<Evenement, bool> filtre = p => p.Confirmed== true && p.P.Firstname=="Herve" && p.P.Lastname=="Inisan";
            var resultats = events
            .Where(filtre)
            .Count();
        }

        [TestMethod]
        public void TestEventsInisan()
        {
            Func<Evenement, bool> filtre = p => p.P.Firstname == "Herve" && p.P.Lastname == "Inisan";
            Func<Evenement, DateTime> tri = p => p.StartDate;
            Func<Evenement, String> l = p => p.Label;
            var resultats = events
            .Where(filtre)
            .OrderBy(tri)
            .ThenBy(l);
        }

        [TestMethod]
        public void TestEventsInisanDerniers()
        {
            Func<Evenement, bool> filtre = p => p.P.Firstname == "Herve" && p.P.Lastname == "Inisan" && p.tags.Contains("Travel");
            Func<Evenement, DateTime> tri = p => p.StartDate;
            Func<Evenement, String> l = p => p.Label;
            var resultats = events
            .Where(filtre)
            .OrderByDescending(tri)
            .Take(3);

        }

        /*[TestMethod]
        public void TestEventsInisanInvites()
        {
            Func<Evenement, bool> filtre = p => p.I > 2;
            var resultats = events
            .Where(filtre);


        }*/


        [TestMethod]
        public void TestBooleanToInt()
        {
            bool a = true;
            bool b = false;
            Assert.AreEqual(1, a.ToInt());
            Assert.AreEqual(0, b.ToInt());
        }

        [TestMethod]
        public void TestBooleanToString()
        {
            bool a = true;
            Assert.AreEqual("Oui", a.ToString(TypeExport.OuiNon));
            Assert.AreEqual("Non", (!a).ToString(TypeExport.OuiNon));
            Assert.AreEqual("Vrai", a.ToString(TypeExport.VraiFaux));
            Assert.AreEqual("Eteint", (!a).ToString(TypeExport.AllumeEteint));
        }


        [TestMethod]
        public void TestOffSet()
        {
            var expected = new List<string>
            {
                "Cinema",
                "Music"
            };

            CollectionAssert.AreEqual(expected, _tags.OffSet(4));
            
        }



    }
}

