﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Extension.Ext;

namespace ExtensionTests
{
    [TestClass]
    public class IntExtensionTest
    {
        #region Tests Int du TP1 

        [TestMethod]
        public void TestIsEven()
        {
            var i = 100;
            Assert.AreEqual(true, i.isEven());
            Assert.IsTrue(i.isEven());
        }

        [TestMethod]
        public void TestIsEvenBis()
        {
            Assert.IsFalse(103.isEven());
        }
        [TestMethod]
        public void TestIsOdd()
        {
            var i = 105;
            Assert.AreEqual(true, i.isOdd());
            Assert.IsTrue(i.isOdd());
        }

        [TestMethod]
        public void TestIsDivisibleBy()
        {
            var i = 100;
            Assert.AreEqual(true, i.IsDivisibleBy(25));
        }
        #endregion

    }
}
