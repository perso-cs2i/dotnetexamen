﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen.Ext
{
    public static class Extension
    {
        public enum TypeExport
        {
            VraiFaux,
            OuiNon,
            AllumeEteint
        }

        public static string ToString(TypeExport type)
        {

            var Res = "";

            switch (type)
            {
                case TypeExport.OuiNon:
                    Res = "Oui";
                    break;

                case TypeExport.VraiFaux:
                    Res = "Vrai";
                    break;

                case TypeExport.AllumeEteint:
                    Res = "Eteint";
                    break;
            }
            return Res;
        }

        public static int ToInt(this bool boolean)
        {
            if (boolean == true)
            {
                return 1;
            }
            else return 0;
        }

        public static List<string> OffSet(this List<string> sourceList, int n)
        {
            var resultList = new List<string>();

            if (n < 0)
            {
                return new Exception;
            }
            else if(n > sourceList.Count)
            {
                return resultList;
            }

            

        }

        
    }
}
