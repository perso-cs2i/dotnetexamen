﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen.Model
{
    public class Invite
    {
        public Invite(string firstname, string lastname, string email, Boolean isAdmin)
        {
            Firstname = firstname;
            Lastname = lastname;
            Email = email;
            IsAdmin = isAdmin;
        }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public Boolean IsAdmin { get; set; }
    }
}
