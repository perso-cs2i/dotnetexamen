﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Examen.Model
{
    public class Evenement
    {
        public Evenement(
            Proprietaire p,
            string label,
            string description,
            DateTime startDate,
            DateTime endDate,
            Boolean confirmed,
            string[] Tags,
            Invite i
            )
        {
            P = p;
            Label = label;
            Description = description;
            StartDate = startDate;
            EndDate = endDate;
            Confirmed = confirmed;
            tags = Tags;
            I = i;
        }


        public Proprietaire P { get; set; }
        public String Label { get; set; }
        public String Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Boolean Confirmed { get; set; }
        public String[] tags { get; set; }

        public Invite I { get; set; }
    }
}
}
